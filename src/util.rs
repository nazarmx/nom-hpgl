/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use types::*;
use nom::{digit};

use std::str;
use std::str::FromStr;

named!(
    pub unsigned_float(&str) -> f32,
    map_res!(
        recognize!(alt!(
            delimited!(digit, complete!(tag!(".")), opt!(complete!(digit))) |
            delimited!(opt!(digit), complete!(tag!(".")), digit) |
            digit
        )),
        FromStr::from_str
    )
);

named!(pub float(&str) -> f32, map!(
    pair!(
        opt!(alt!(tag!("+") | tag!("-"))),
        unsigned_float
    ),
    |(sign, value): (Option<&str>, f32)| {
        sign.and_then(|s| if &s[0..1] == "-" { Some(-1f32) } else { None }).unwrap_or(1f32) * value
    }
));

fn is_digit(c: char) -> bool {
    c.is_digit(10)
}

named!(pub parse_i32(&str) -> i32,
    flat_map!(
        recognize!(
            preceded!(
                opt!(one_of!("+-")),
                take_while1_s!(is_digit)
            )
        ),
        parse_to!(i32)
    )
);

named!(pub parse_i16(&str) -> i16,
    flat_map!(
        recognize!(
            preceded!(
                opt!(one_of!("+-")),
                take_while1!(is_digit)
            )
        ),
        parse_to!(i16)
    )
);

named!(pub parse_u8(&str) -> u8,
    flat_map!(
        recognize!(
            take_while1!(is_digit)
        ),
        parse_to!(u8)
    )
);

named!(pub parse_bool(&str) -> bool,
    map!(
        alt_complete!(
            char!('0') |
            char!('1')
        ),
        |c| c == '1'
    )
);

named!(pub point(&str) -> Point,
    ws!(
        separated_pair!(
            float,
            char!(','),
            float
        )
    )
);

named!(pub points(&str) -> Vec<Point>,
    ws!(
        separated_list_complete!(
            char!(','),
            point
        )
    )
);

#[cfg(test)]
mod tests {
    use super::*;
    use nom::IResult;
    
    #[test]
    fn parse_point_whitespace() {
        assert_eq!(
            point("123.0, 123.0"),
            IResult::Done("", (123.0, 123.0))
        );
        assert_eq!(
            point(" 123,123 "),
            IResult::Done("", (123.0, 123.0))
        );
        assert_eq!(
            point("+123, -123"),
            IResult::Done("", (123.0, -123.0))
        );
        assert_eq!(
            point(" -123,+123 "),
            IResult::Done("", (-123.0, 123.0))
        );
    }

    #[test]
    fn parse_point_no_whitespace() {
        assert_eq!(
            point("123,123"),
            IResult::Done("", (123.0, 123.0))
        );
        assert_eq!(
            point("+123,+123"),
            IResult::Done("", (123.0, 123.0))
        );
        assert_eq!(
            point("-123,-123"),
            IResult::Done("", (-123.0, -123.0))
        );
        assert_eq!(
            point("+123,-123"),
            IResult::Done("", (123.0, -123.0))
        );
    }

    #[test]
    fn parse_points() {
        assert_eq!(
            points("123, 123, 123, 123"),
            IResult::Done("", vec![(123.0, 123.0), (123.0, 123.0)])
        );
        assert_eq!(
            points(" 123,123, 123,123"),
            IResult::Done("", vec![(123.0, 123.0), (123.0, 123.0)])
        );
    }
}
