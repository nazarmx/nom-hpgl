/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use super::*;

#[derive(Debug, Clone, PartialEq)]
pub enum Vector {
    ArcAbsolute {
        center: Point,
        sweep_angle: f32,
        chord_angle: Option<f32>,
    },
    ArcRelative {
        increments: Point,
        sweep_angle: f32,
        chord_angle: Option<f32>,
    },
    AbsoluteArcThreePoint {
        intermediate: Point,
        end: Point,
        chord_angle: Option<f32>,
    },
    Circle {
        radius: f32,
        chord_angle: Option<f32>,
    },
    PlotAbsolute(Vec<Point>),
    PenDown(Vec<Point>),
    PolylineEncoded(Vec<Point>),
    PlotRelative(Vec<Point>),
    PenUp(Vec<Point>),
    RelativeArcThreePoint {
        intermediate: Point,
        end: Point,
        chord_angle: Option<f32>,
    },
}
