/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use super::*;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum FillType {
    SolidBidirectional,
    SolidUnidirectional,
    Hatched {
        spacing: f32,
        angle: f32,
    },
    CrossHatched {
        spacing: f32,
        angle: f32,
    },
    Shading {
        shading_level: f32
    },
    UserDefinedRF {
        index: u8,
        pen_flag: Option<bool>,
    },
    PredefinedImportedPCL(u8),
    UserDefinedPCL(u8),
}

impl Default for FillType {
    fn default() -> FillType {
        FillType::SolidBidirectional
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum LineFillAttribute {
    AnchorCorner(Option<Point>),
    FillType(Option<u8>),
    SelectPen(i32),
    LineType {
        line_type: Option<i32>,
        pattern_length: f32,
        mode: bool,
    },
}
