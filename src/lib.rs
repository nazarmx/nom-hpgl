/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
#[macro_use]
extern crate nom;

mod types;

pub use types::*;

mod util;
mod commands;

use commands::*;

named!(pub parse(&str) -> Vec<Command>,
    separated_list_complete!(
        one_of!(";\x03"),
        ws!(command)
    )
);

/*
#[cfg(test)]
mod tests {
    use super::*;
    use std::convert::Into;
    use nom::IResult;

    const EMPTY_U8_SLICE: &'static [u8] = &[];

    #[test]
    fn dummy_parse_works() {
        assert_eq!(
            parse(
                br#" IN; DF;CO"Creation Date: 28-07-2017"; IN; SC ; IP1,2,3,4; IN"#,
            ),
            IResult::Done(
                EMPTY_U8_SLICE,
                vec![
                    Command::Initialize,
                    Command::Default,
                    Command::Comment("Creation Date: 28-07-2017".into()),
                    Command::Initialize,
                    Command::Scale,
                    Command::InputP1P2(((1, 2), (3, 4))),
                    Command::Initialize,
                ],
            )
        );
    }

}
*/
