/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use super::Command;
use types::LineFillAttribute;
use util::*;

named!(anchor_corner(&str) -> LineFillAttribute,
    ws!(do_parse!(
        tag!("AC") >>
        point: opt!(complete!(point)) >>
        (LineFillAttribute::AnchorCorner(point))
    ))
);

named!(fill_type(&str) -> LineFillAttribute,
    ws!(do_parse!(
        tag!("FT") >>
        fill_type: opt!(complete!(parse_u8)) >>
        (LineFillAttribute::FillType(fill_type))
    ))
);

named!(select_pen(&str) -> LineFillAttribute,
    ws!(do_parse!(
        tag!("SP") >>
        pen_number: map!(
            opt!(complete!(parse_i32)),
            |x: Option<i32>| x.unwrap_or(0)
        ) >>
        (LineFillAttribute::SelectPen(pen_number))
    ))
);

named!(line_type(&str) -> LineFillAttribute,
    ws!(do_parse!(
        tag!("LT") >>
        line_type: opt!(complete!(parse_i32)) >>
        pattern_length: map!(
            opt!(preceded!(
                complete!(char!(',')),
                unsigned_float
            )),
            |x: Option<f32>| x.unwrap_or(4.0)
        ) >>
        mode: map!(
            opt!(preceded!(
                complete!(char!(',')),
                parse_bool
            )),
            |x: Option<bool>| x.unwrap_or(false)
        ) >>
        (LineFillAttribute::LineType { line_type, pattern_length, mode })
    ))
);

named!(pub line_fill_attribute(&str) -> Command,
    map!(
        alt_complete!(
            anchor_corner |
            fill_type |
            select_pen |
            line_type
        ),
        |x| Command::LineFillAttribute(x)
    )
);

#[cfg(test)]
mod tests {
    use nom;
    use super::*;

    #[test]
    fn parse_anchor_corner() {
        assert_eq!(
            line_fill_attribute("AC 3000,1500"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::AnchorCorner(
                    Some((3000.0, 1500.0))
                )),
            )
        );
        assert_eq!(
            line_fill_attribute("AC"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::AnchorCorner(
                    None
                )),
            )
        );
    }

    #[test]
    fn parse_fill_type() {
        assert_eq!(
            line_fill_attribute("FT"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::FillType(
                    None
                )),
            )
        );

        assert_eq!(
            line_fill_attribute("FT 1"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::FillType(
                    Some(1u8)
                )),
            )
        );
    }

    #[test]
    fn parse_select_pen() {
        assert_eq!(
            line_fill_attribute("SP"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::SelectPen(
                    0
                )),
            )
        );

        assert_eq!(
            line_fill_attribute("SP1"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::SelectPen(
                    1
                )),
            )
        );
    }

    #[test]
    fn parse_line_type() {
        assert_eq!(
            line_fill_attribute("LT"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::LineType {
                    line_type: None,
                    pattern_length: 4.0,
                    mode: false,
                }),
            )
        );
        assert_eq!(
            line_fill_attribute("LT99"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::LineType {
                    line_type: Some(99),
                    pattern_length: 4.0,
                    mode: false,
                }),
            )
        );
        assert_eq!(
            line_fill_attribute("LT 12,1.5,1"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::LineType {
                    line_type: Some(12),
                    pattern_length: 1.5,
                    mode: true,
                }),
            )
        );
        assert_eq!(
            line_fill_attribute("LT -8,1.5"),
            nom::IResult::Done(
                "",
                Command::LineFillAttribute(LineFillAttribute::LineType {
                    line_type: Some(-8),
                    pattern_length: 1.5,
                    mode: false,
                }),
            )
        );
    }
}
