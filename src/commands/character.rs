/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

use super::Command;
use types::{Character, Point};
use util::*;

named!(absolute_direction(&str) -> Character,
    ws!(do_parse!(
        tag!("DI") >>
        point: map!(
            opt!(complete!(point)),
            |x: Option<Point>| x.unwrap_or((1.0, 0.0))
        ) >>
        (Character::AbsoluteDirection(point))
    ))
);

named!(absolute_character_size(&str) -> Character,
    ws!(do_parse!(
        tag!("SI") >>
        point: opt!(complete!(point)) >>
        (Character::AbsoluteCharacterSize(point))
    ))
);

named!(label(&str) -> Character,
    ws!(do_parse!(
        tag!("LB") >>
        text: take_until_s!("\x03") >>
        (Character::Label(text))
    ))
);

named!(pub character(&str) -> Command,
    map!(
        alt_complete!(
            absolute_direction |
            absolute_character_size |
            label
        ),
        |x| Command::Character(x)
    )
);

#[cfg(test)]
mod tests {
    use nom;
    use super::*;

    #[test]
    fn parse_absolute_direction() {
        assert_eq!(
            character("DI 10.0, -2.3"),
            nom::IResult::Done(
                "",
                Command::Character(Character::AbsoluteDirection(
                    (10.0, -2.3)
                )),
            )
        );
        assert_eq!(
            character("DI"),
            nom::IResult::Done(
                "",
                Command::Character(Character::AbsoluteDirection(
                    (1.0, 0.0)
                )),
            )
        );
    }

    #[test]
    fn parse_absolute_character_size() {
        assert_eq!(
            character("SI"),
            nom::IResult::Done(
                "",
                Command::Character(Character::AbsoluteCharacterSize(
                    None
                )),
            )
        );
        assert_eq!(
            character("SI0.465667,0.705556"),
            nom::IResult::Done(
                "",
                Command::Character(Character::AbsoluteCharacterSize(
                    Some((0.465667, 0.705556))
                )),
            )
        );
    }

    #[test]
    fn parse_label() {
        let label = "Hello, world!";
        let command = format!("LB{}\x03", label);

        assert_eq!(
            character(&command),
            nom::IResult::Done(
                "\x03",
                Command::Character(Character::Label(label)),
            )
        );
    }
}
