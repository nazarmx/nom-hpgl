/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use nom;
use super::Command;
use types::ConfigStatus;
use util::*;

fn is_factory_initialize(val: Option<f32>) -> bool {
    if let Some(x) = val {
        x.round() == 1.0
    } else {
        false
    }
}

named!(initialize(&str) -> ConfigStatus,
    ws!(do_parse!(
        tag!("IN") >>
        is_factory: map!(
            opt!(complete!(float)),
            is_factory_initialize
        ) >>
        (ConfigStatus::Initialize(is_factory))
    ))
);

named!(comment(&str) -> ConfigStatus,
    ws!(do_parse!(
        tag!("CO") >>
        text: delimited!(
            tag!("\""),
            take_until_s!("\""),
            tag!("\"")
        ) >>
        (ConfigStatus::Comment(text))
    ))
);

named!(define_terminator(&str) -> ConfigStatus,
    ws!(do_parse!(
        tag!("DT") >>
        label_terminator: map!(
            opt!(complete!(nom::anychar)),
            |c| c.unwrap_or('\x03')
        ) >>
        mode: map!(
            opt!(preceded!(
                complete!(char!(',')),
                parse_u8
            )),
            |c| c.unwrap_or(1) == 1
        ) >>
        (ConfigStatus::DefineLabelTerminator { label_terminator, mode })
    ))
);

named!(advance_full_page(&str) -> ConfigStatus,
    ws!(do_parse!(
        tag!("PG") >>
        n: opt!(complete!(parse_i16)) >>
        (ConfigStatus::AdvanceFullPage(n))
    ))
);

named!(pub config_status(&str) -> Command,
    map!(
        alt_complete!(
            initialize |
            comment |
            define_terminator |
            advance_full_page
        ),
        |x| Command::ConfigStatus(x)
    )
);

#[cfg(test)]
mod tests {
    use nom;
    use super::*;

    #[test]
    fn parse_initialize() {
        assert_eq!(
            config_status("IN"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::Initialize(false)),
            )
        );
        assert_eq!(
            config_status("IN1"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::Initialize(true)),
            )
        );
        assert_eq!(
            config_status("IN0.6"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::Initialize(true)),
            )
        );
        assert_eq!(
            config_status("IN 0.2"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::Initialize(false)),
            )
        );
    }

    #[test]
    fn parse_comment() {
        let comment = "Hello, world!";
        let command = format!("CO \"{}\"", comment);

        assert_eq!(
            config_status(&command),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::Comment(comment)),
            )
        );
    }

    #[test]
    fn parse_define_terminator() {
        assert_eq!(
            config_status("DT"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::DefineLabelTerminator {
                    label_terminator: '\x03',
                    mode: true,
                }),
            )
        );
        assert_eq!(
            config_status("DT%"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::DefineLabelTerminator {
                    label_terminator: '%',
                    mode: true,
                }),
            )
        );
        assert_eq!(
            config_status("DT%,0"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::DefineLabelTerminator {
                    label_terminator: '%',
                    mode: false,
                }),
            )
        );
    }

    #[test]
    fn parse_advance_full_page() {
        assert_eq!(
            config_status("PG"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::AdvanceFullPage(
                    None
                )),
            )
        );
        assert_eq!(
            config_status("PG15"),
            nom::IResult::Done(
                "",
                Command::ConfigStatus(ConfigStatus::AdvanceFullPage(
                    Some(15)
                )),
            )
        );
    }
}
