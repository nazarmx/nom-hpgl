/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

use super::Command;
use types::AdvancedText;
use util::*;

named!(label_mode(&str) -> AdvancedText,
    ws!(do_parse!(
        tag!("LM") >>
        mode: map!(
            opt!(complete!(parse_bool)),
            |x: Option<bool>| x.unwrap_or(false)
        ) >>
        row_number: map!(
            opt!(preceded!(
                complete!(char!(',')),
                parse_u8
            )),
            |x: Option<u8>| x.unwrap_or(0)
        ) >>
        (AdvancedText::LabelMode { mode, row_number })
    ))
);

named!(pub advanced_text(&str) -> Command,
    map!(
        alt_complete!(
            label_mode
        ),
        |x| Command::AdvancedText(x)
    )
);

#[cfg(test)]
mod tests {
    use nom;
    use super::*;

    #[test]
    fn parse_label_mode() {
        assert_eq!(
            advanced_text("LM"),
            nom::IResult::Done(
                "",
                Command::AdvancedText(AdvancedText::LabelMode {
                    mode: false,
                    row_number: 0,
                }),
            )
        );
        assert_eq!(
            advanced_text("LM1"),
            nom::IResult::Done(
                "",
                Command::AdvancedText(AdvancedText::LabelMode {
                    mode: true,
                    row_number: 0,
                }),
            )
        );
        assert_eq!(
            advanced_text("LM0,127"),
            nom::IResult::Done(
                "",
                Command::AdvancedText(AdvancedText::LabelMode {
                    mode: false,
                    row_number: 127,
                }),
            )
        );
    }
}
