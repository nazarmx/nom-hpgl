/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use types::*;

mod vector;

use self::vector::*;

mod line_fill_attribute;

pub use self::line_fill_attribute::*;

mod config_status;

pub use self::config_status::*;

mod advanced_text;

pub use self::advanced_text::*;

mod character;

pub use self::character::*;

named!(pub command(&str) -> Command,
    alt_complete!(
        vector |
        line_fill_attribute |
        config_status |
        advanced_text |
        character
    )
);
