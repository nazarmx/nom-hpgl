/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
use super::Command;
use types::Vector;
use util::*;

named!(arc_absolute(&str) -> Vector,
    ws!(do_parse!(
        tag!("AA") >>
        center: point >>
        char!(',') >>
        sweep_angle: float >>
        chord_angle: opt!(preceded!(
            complete!(char!(',')),
            unsigned_float
        )) >>
        (Vector::ArcAbsolute { center, sweep_angle, chord_angle })
    ))
);

named!(arc_relative(&str) -> Vector,
    ws!(do_parse!(
        tag!("AR") >>
        increments: point >>
        char!(',') >>
        sweep_angle: float >>
        chord_angle: opt!(preceded!(
            complete!(char!(',')),
            unsigned_float
        )) >>
        (Vector::ArcRelative { increments, sweep_angle, chord_angle })
    ))
);

named!(absolute_arc_three_point(&str) -> Vector,
    ws!(do_parse!(
        tag!("AT") >>
        intermediate: point >>
        char!(',') >>
        end: point >>
        chord_angle: opt!(preceded!(
            complete!(char!(',')),
            unsigned_float
        )) >>
        (Vector::AbsoluteArcThreePoint { intermediate, end, chord_angle })
    ))
);

named!(circle(&str) -> Vector,
    ws!(do_parse!(
        tag!("CI") >>
        radius: float >>
        chord_angle: opt!(preceded!(
            complete!(char!(',')),
            unsigned_float
        )) >>
        (Vector::Circle { radius, chord_angle })
    ))
);

named!(plot_absolute(&str) -> Vector,
    ws!(do_parse!(
        tag!("PA") >>
        points: points >>
        opt!(preceded!(
            complete!(char!(',')),
            float
        )) >>
        (Vector::PlotAbsolute (points))
    ))
);

named!(pen_down(&str) -> Vector,
    ws!(do_parse!(
        tag!("PD") >>
        points: points >>
        opt!(preceded!(
            complete!(char!(',')),
            float
        )) >>
        (Vector::PenDown (points))
    ))
);

named!(polyline_encoded(&str) -> Vector,
    ws!(do_parse!(
        tag!("PE") >>
        (Vector::PolylineEncoded (vec![]))
    ))
);

named!(plot_relative(&str) -> Vector,
    ws!(do_parse!(
        tag!("PR") >>
        points: points >>
        opt!(preceded!(
            complete!(char!(',')),
            float
        )) >>
        (Vector::PlotRelative (points))
    ))
);

named!(pen_up(&str) -> Vector,
    ws!(do_parse!(
        tag!("PU") >>
        points: points >>
        opt!(preceded!(
            complete!(char!(',')),
            float
        )) >>
        (Vector::PenUp (points))
    ))
);

named!(relative_arc_three_point(&str) -> Vector,
    ws!(do_parse!(
        tag!("RT") >>
        intermediate: point >>
        char!(',') >>
        end: point >>
        chord_angle: opt!(preceded!(
            complete!(char!(',')),
            unsigned_float
        )) >>
        (Vector::RelativeArcThreePoint { intermediate, end, chord_angle })
    ))
);

named!(pub vector(&str) -> Command,
    map!(
        alt_complete!(
            arc_absolute |
            arc_relative |
            absolute_arc_three_point |
            circle |
            plot_absolute |
            pen_down |
            polyline_encoded |
            plot_relative |
            pen_up |
            relative_arc_three_point
        ),
        |x| Command::Vector(x)
    )
);

#[cfg(test)]
mod tests {
    use nom;
    use super::*;

    #[test]
    fn parse_arc_absolute() {
        assert_eq!(
            vector("AA 0,0,45"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::ArcAbsolute {
                    center: (0.0, 0.0),
                    sweep_angle: 45.0,
                    chord_angle: None,
                }),
            )
        );
        assert_eq!(
            vector("AA 0,0,45,25"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::ArcAbsolute {
                    center: (0.0, 0.0),
                    sweep_angle: 45.0,
                    chord_angle: Some(25.0),
                }),
            )
        );
    }

    #[test]
    fn parse_arc_relative() {
        assert_eq!(
            vector("AR 0,2000,80,25"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::ArcRelative {
                    increments: (0.0, 2000.0),
                    sweep_angle: 80.0,
                    chord_angle: Some(25.0),
                }),
            )
        );
        assert_eq!(
            vector("AR 2000,0,80"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::ArcRelative {
                    increments: (2000.0, 0.0),
                    sweep_angle: 80.0,
                    chord_angle: None,
                }),
            )
        );
    }

    #[test]
    fn parse_absolute_arc_three_point() {
        assert_eq!(
            vector("AT 3200,800,2500,100"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::AbsoluteArcThreePoint {
                    intermediate: (3200.0, 800.0),
                    end: (2500.0, 100.0),
                    chord_angle: None,
                }),
            )
        );
        assert_eq!(
            vector("AT 3300,800,3200,700,13"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::AbsoluteArcThreePoint {
                    intermediate: (3300.0, 800.0),
                    end: (3200.0, 700.0),
                    chord_angle: Some(13.0),
                }),
            )
        );
    }

    #[test]
    fn parse_circle() {
        assert_eq!(
            vector("CI 750,45"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::Circle {
                    radius: 750.0,
                    chord_angle: Some(45.0),
                }),
            )
        );
        assert_eq!(
            vector("CI 750"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::Circle {
                    radius: 750.0,
                    chord_angle: None,
                }),
            )
        );
    }

    #[test]
    fn parse_plot_absolute() {
        assert_eq!(
            vector("PA"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PlotAbsolute(
                    vec![]
                )),
            )
        );
        assert_eq!(
            vector("PA 10,20,12,15"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PlotAbsolute(
                    vec![(10.0, 20.0), (12.0, 15.0)]
                )),
            )
        );
        assert_eq!(
            vector("PA 10,20,12"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PlotAbsolute(
                    vec![(10.0, 20.0)]
                )),
            )
        );
    }

    #[test]
    fn parse_pen_down() {
        assert_eq!(
            vector("PD"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PenDown(
                    vec![]
                )),
            )
        );
        assert_eq!(
            vector("PD 2500,10,10,1500,10,10"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PenDown(
                    vec![(2500.0, 10.0), (10.0, 1500.0), (10.0, 10.0)]
                )),
            )
        );
        assert_eq!(
            vector("PD 10,20,12"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PenDown(
                    vec![(10.0, 20.0)]
                )),
            )
        );
    }

    #[test]
    fn parse_plot_relative() {
        assert_eq!(
            vector("PR"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PlotRelative(
                    vec![]
                )),
            )
        );
        assert_eq!(
            vector("PR 2500,0,-2500,1500,0,-150"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PlotRelative(
                    vec![(2500.0, 0.0), (-2500.0, 1500.0), (0.0, -150.0)]
                )),
            )
        );
        assert_eq!(
            vector("PR 10,20,12"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PlotRelative(
                    vec![(10.0, 20.0)]
                )),
            )
        );
    }

    #[test]
    fn parse_pen_up() {
        assert_eq!(
            vector("PU"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PenUp(
                    vec![]
                )),
            )
        );
        assert_eq!(
            vector("PU 2500,0,-2500,1500,0,-150"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PenUp(
                    vec![(2500.0, 0.0), (-2500.0, 1500.0), (0.0, -150.0)]
                )),
            )
        );
        assert_eq!(
            vector("PU 10,20,12"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::PenUp(
                    vec![(10.0, 20.0)]
                )),
            )
        );
    }

    #[test]
    fn parse_relative_arc_three_point() {
        assert_eq!(
            vector("RT 3200,800,2500,100"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::RelativeArcThreePoint {
                    intermediate: (3200.0, 800.0),
                    end: (2500.0, 100.0),
                    chord_angle: None,
                }),
            )
        );
        assert_eq!(
            vector("RT 3300,800,3200,700,13"),
            nom::IResult::Done(
                "",
                Command::Vector(Vector::RelativeArcThreePoint {
                    intermediate: (3300.0, 800.0),
                    end: (3200.0, 700.0),
                    chord_angle: Some(13.0),
                }),
            )
        );
    }
}
