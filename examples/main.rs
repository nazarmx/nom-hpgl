/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

extern crate nom_hpgl;
extern crate memmap;
extern crate nom;
extern crate encoding;
use std::env;
use nom::IResult;
use nom_hpgl::Command;
use memmap::{Mmap, Protection};
use encoding::{DecoderTrap};

fn main() {
    let arg = env::args().nth(1);
    let path = arg.unwrap();

    let mmap = Mmap::open_path(path, Protection::Read).unwrap();

    let enc: Option<String> = env::args().nth(2);

    let mut encoded = String::new();

    let text: &str = unsafe {
        if let Some(encoding_name) = enc {
            let encoding = encoding::label::encoding_from_whatwg_label(&encoding_name).unwrap();
            encoding.decode_to(mmap.as_slice(), DecoderTrap::Strict, &mut encoded)
                .unwrap();
            &encoded
        } else {
            std::str::from_utf8(mmap.as_slice()).unwrap()
        }
    };

    let parsed: IResult<&str, Vec<Command>> = nom_hpgl::parse(text);

    if let IResult::Done(remaining, data) = parsed {
        let len = std::cmp::min(10, remaining.len());
        let peek: &str = &remaining[0..len];

        println!("{} bytes unparsed, next unparsed command: {}", remaining.len(), peek.trim_left_matches(';'));
        if let Some(last_item) = data.last() {
            println!("last command: {:?}", last_item);
        }
    };
}
