/*
 *  Copyright 2017 Nazar Mishturak
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

extern crate nom_hpgl;
extern crate memmap;
extern crate nom;
extern crate svg;
extern crate rayon;
use rayon::prelude::*;
use std::env;
use nom::IResult;
use nom_hpgl::{Command, Character, ConfigStatus, LineFillAttribute, Vector, Point};
use memmap::{Mmap, Protection};
use svg::{Document, Node};
use svg::node::element::{path, Path, Text};

const UNIT_TO_MILIMETER_RATIO: f32 = 1.0 / 40.0;

#[inline]
fn milimeters(xy: Point) -> Point {
    (
        xy.0 as f32 * UNIT_TO_MILIMETER_RATIO,
        xy.1 as f32 * UNIT_TO_MILIMETER_RATIO,
    )
}

#[inline]
fn f_max(a: f32, b: f32) -> f32 {
    if a > b {
        a
    } else {
        b
    }
}

fn set_line_type_params(p: &mut Path, pattern_len: f32) {
    let dash_percent = format!("{}%", pattern_len);
    p.assign("stroke-dasharray", dash_percent);
}

fn convert<P>(p: P)
    where
        P: AsRef<std::path::Path>,
{
    let path = p.as_ref();
    let mmap = Mmap::open_path(path, Protection::Read).unwrap();
    let text: &str = unsafe {
        let slice = mmap.as_slice();
        std::str::from_utf8(slice).unwrap()
    };

    let result: IResult<&str, Vec<Command>> = nom_hpgl::parse(text);
    let (_, data) = result.unwrap();

    let mut is_path_empty = true;
    let mut svg_path_data = path::Data::new().move_to((0, 0));
    let (mut x_max, mut y_max) = (0.0, 0.0);
    let mut last_xy = (0.0, 0.0);

    let mut line_type_params: Option<_> = Some((0i32, 0.0f32));
    let mut font_size_cm = (0.285, 0.375);
    let mut document = Document::new()
        // https://stackoverflow.com/a/17099730/2502409
        // Set monospace font
        .set("font-family", "monospace");

    for command in data.iter() {
        match *command {
            Command::Character(ref character_command) => {
                if let &Character::Label(ref label) = character_command {
                    let text: String = label.trim().to_string();
                    let text_node = svg::node::Text::new(text);
                    let mut text_element = Text::new();
                    text_element.append(text_node);
                    text_element.assign("x", last_xy.0);
                    text_element.assign("y", last_xy.1);

                    // https://stackoverflow.com/a/17099730/2502409
                    // Set only the height
                    let font_size_mm = format!("{}", font_size_cm.1 * 10.0);
                    text_element.assign("font-size", font_size_mm);

                    document.append(text_element);
                } else if let &Character::AbsoluteCharacterSize(ref opt_size) = character_command {
                    if let &Some(size) = opt_size {
                        font_size_cm = size;
                    }
                }
            }
            Command::ConfigStatus(ref config_command) => {
                if let &ConfigStatus::Comment(ref comment_text) = config_command {
                    let comment = format!("<!-- {} -->", comment_text);
                    let text_node = svg::node::Text::new(comment);
                    document.append(text_node);
                }
            }
            Command::Vector(ref vector_command) => {
                if let &Vector::PenUp(ref points) = vector_command {
                    for p in points.iter().cloned() {
                        x_max = f_max(x_max, p.0);
                        y_max = f_max(y_max, p.1);
                        last_xy = milimeters(p);
                        svg_path_data = svg_path_data.move_to(last_xy);
                    }
                }
                else if let &Vector::PenDown(ref points) = vector_command {
                    for p in points.iter().cloned() {
                        x_max = f_max(x_max, p.0);
                        y_max = f_max(y_max, p.1);
                        last_xy = milimeters(p);
                        svg_path_data = svg_path_data.line_to(last_xy).move_to(last_xy);
                        if is_path_empty {
                            is_path_empty = false;
                        }
                    }
                }
            }
            Command::LineFillAttribute(ref line_fill_attr) => {
                if let &LineFillAttribute::LineType { line_type, pattern_length, mode: _mode } = line_fill_attr {
                    let new_params = if let Some(pattern_type) = line_type {
                        Some((pattern_type, pattern_length))
                    } else {
                        None
                    };

                    if line_type_params != new_params {
                        if !is_path_empty {
                            svg_path_data = svg_path_data.close();
                            let mut current_path = Path::new()
                                .set("fill", "none")
                                .set("stroke-width", "0.5mm")
                                .set("stroke", "black")
                                .set("d", svg_path_data);
                            if let Some(params) = line_type_params {
                                set_line_type_params(&mut current_path, params.1);
                            }
                            document.append(current_path);
                            svg_path_data = path::Data::new().move_to(last_xy);
                            is_path_empty = true;
                        }

                        line_type_params = new_params;
                    }
                }
            }
            _ => {}
        }
    }

    if !is_path_empty {
        svg_path_data = svg_path_data.close();
        let mut remaining_path = Path::new()
            .set("fill", "none")
            .set("stroke", "black")
            .set("stroke-width", "0.5mm")
            .set("d", svg_path_data);
        if let Some(params) = line_type_params {
            set_line_type_params(&mut remaining_path, params.1);
        }
        document.append(remaining_path);
    }
    let x_max_mm = (x_max as f32) * UNIT_TO_MILIMETER_RATIO;
    let y_max_mm = (y_max as f32) * UNIT_TO_MILIMETER_RATIO;

    let width = format!("{}mm", x_max_mm);
    let height = format!("{}mm", y_max_mm);
    let view_box = format!("0 0 {} {}", x_max_mm, y_max_mm);

    document.assign("viewBox", view_box);
    document.assign("width", width.clone());
    document.assign("height", height.clone());

    let out = path.with_extension("svg");
    svg::save(out, &document).unwrap();
}

fn main() {
    let files = env::args_os().skip(1).collect::<Vec<_>>();

    files.into_par_iter().for_each(convert);
}
