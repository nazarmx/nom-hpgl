## Rust HP-GL parser using nom [![build status](https://gitlab.com/nazarmx/nom-hpgl/badges/master/build.svg)](https://gitlab.com/nazarmx/nom-hpgl/commits/master) [![crates.io](https://img.shields.io/crates/v/nom-hpgl.svg)](https://img.shields.io/crates/v/nom-hpgl.svg) [![License](https://img.shields.io/badge/License-Apache%202.0-blue.svg)](https://opensource.org/licenses/Apache-2.0) 

[Documentation](https://nazarmx.gitlab.io/nom-hpgl/nom_hpgl/)
